import { scaleOrdinal } from '@vx/scale';
import { LegendOrdinal } from '@vx/legend';
 
import { color as colors } from '@data-ui/theme';
import { RadialChart, ArcSeries, ArcLabel } from '@data-ui/radial-chart';
 
const colorScale = scaleOrdinal({ range: colors.categories });
const data = [{ label: 'a', value: 200 }, { label: 'c', value: 150 }, { label: 'c', value: 21 }];
 
export default () => (
  <div style={{ display: 'flex', alignItems: 'center' }}>
    <RadialChart
      ariaLabel="This is a radial-chart chart of..."
      width={width}
      height={height}
      margin={{ top, right, bottom, left }}
      renderTooltip={({ event, datum, data, fraction }) => (
        <div>
          <strong>{datum.label}</strong>
          {datum.value} ({(fraction * 100).toFixed(2)}%)
        </div>
      )}
    >
      <ArcSeries
        data={data}
        pieValue={d => d.value}
        fill={arc => colorScale(arc.data.label)}
        stroke="#fff"
        strokeWidth={1}
        label{arc => `${(arc.data.value).toFixed(1)}%`}
        labelComponent={<ArcLabel />}
        innerRadius={radius => 0.35 * radius}
        outerRadius={radius => 0.6 * radius}
        labelRadius={radius => 0.75 * radius}
      />
    </RadialChart>
    <LegendOrdinal
      direction="column"
      scale={colorScale}
      shape="rect"
      fill={({ datum }) => colorScale(datum)}
      labelFormat={label => label}
    />
  </div>
);