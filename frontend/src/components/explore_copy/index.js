import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getIris } from "../../actions/iris";

import C3Chart from 'react-c3js';
import 'c3/c3.css';

import {
  XYChart,
  PointSeries,
  XAxis as XYChartXAxis,
  YAxis as XYChartYAxis,
  CrossHair,
  theme,
  withParentSize,
} from '@data-ui/xy-chart';

import { genRandomNormalPoints } from '@vx/mock-data';

import { Histogram, WrappedComponent, PatternLines,
   DensitySeries, BarSeries, XAxis, YAxis } from '@data-ui/histogram';
import {PatternLines as XYChartPatternLines, AreaSeries} from "@data-ui/xy-chart";

import { timeParse, timeFormat } from "d3-time-format";

export const parseDate = timeParse("%Y%m%d");
export const formatHour = timeFormat("%I:%M");
export const formatDay = timeFormat("%b %d");
export const formatVerbose = timeFormat("%B %d, %Y");


import Checkbox from '../shared/Checkbox';

const BIN_COUNT = 50;
const n = 10;
const datasets = [];
const datasetColors = theme.colors.categories;

export const pointData = genRandomNormalPoints(n).forEach(([x, y], i) => {
  const dataSetIndex = Math.floor(i / n);
  if (!datasets[dataSetIndex]) datasets[dataSetIndex] = [];

  datasets[dataSetIndex].push({
    x: dataSetIndex === 1 ? x : x + Math.random(),
    y: dataSetIndex === 1 ? y - Math.random() : y,
    fill: theme.colors.categories[dataSetIndex],
    size: Math.max(3, Math.random() * 10),
  });
});

const marginScatter = { top: 10, right: 10, bottom: 64, left: 64 };

const marginTopHist = {
  top: 10,
  right: marginScatter.right,
  bottom: 5,
  left: marginScatter.left,
};

const marginSideHist = {
  top: 10,
  right: marginScatter.bottom,
  bottom: 5,
  left: marginScatter.top,
};

function renderTooltip({ datum }) {
  const { x, y, fill: color } = datum;

  return (
    <div>
      <div>
        <strong style={{ color }}>x </strong>
        {x.toFixed(2)}
      </div>
      <div>
        <strong style={{ color }}>y </strong>
        {y.toFixed(2)}
      </div>
    </div>
  );
}

const propTypes = {
  parentWidth: PropTypes.number.isRequired,
};

const defaultProps = {};

const timeSeriesData = [
  { x: new Date("2017-01-01 07:59"), y: Math.random() },
  { x: new Date("2017-01-01 09:15"), y: Math.random() },
  { x: new Date("2017-01-01 10:31"), y: Math.random() },
  { x: new Date("2017-01-01 11:01"), y: Math.random() },
  { x: new Date("2017-01-01 12:00"), y: Math.random() },
  { x: new Date("2017-01-01 13:00"), y: Math.random() },
  { x: new Date("2017-01-01 14:00"), y: Math.random() },
  { x: new Date("2017-01-01 15:00"), y: Math.random() },
  { x: new Date("2017-01-01 16:00"), y: Math.random() }
];

export class IrisExplore extends Component {
  static propTypes = {
    iris: PropTypes.array.isRequired,
    getIris: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { showVoronoi: false };
  }

  renderScatter({ width, height }) {
    console.log('rendering scatter');
    console.log(      <div style={{ transform: 'rotate(90)' }}>
    <XYChart
      ariaLabel="X- and y- values"
      width={width}
      height={height}
      xScale={{ type: 'linear' }}
      yScale={{ type: 'linear' }}
      margin={marginScatter}
      theme={theme}
      renderTooltip={renderTooltip}
      eventTrigger="voronoi"
      showVoronoi={this.state.showVoronoi}
    >
      {datasets.map((dataset, i) => (
        <PointSeries key={i} data={dataset} fill={datasetColors[i]} opacity={0.7} size={5} />
      ))}
      <CrossHair
        stroke={theme.colors.grays[6]}
        circleFill="transparent"
        circleSize={8}
        circleStroke={theme.colors.grays[6]}
        fullWidth
        fullHeight
      />
      <XYChartXAxis label="x value" />
      <XYChartYAxis label="y value" orientation="left" />
    </XYChart>
  </div>);


    return (
      <div style={{ transform: 'rotate(90)' }}>
        <XYChart
          ariaLabel="X- and y- values"
          width={width}
          height={height}
          xScale={{ type: 'linear' }}
          yScale={{ type: 'linear' }}
          margin={marginScatter}
          theme={theme}
          renderTooltip={renderTooltip}
          eventTrigger="voronoi"
          showVoronoi={this.state.showVoronoi}
        >
          {datasets.map((dataset, i) => (
            <PointSeries key={i} data={dataset} fill={datasetColors[i]} opacity={0.7} size={5} />
          ))}
          <CrossHair
            stroke={theme.colors.grays[6]}
            circleFill="transparent"
            circleSize={8}
            circleStroke={theme.colors.grays[6]}
            fullWidth
            fullHeight
          />
          <XYChartXAxis label="x value" />
          <XYChartYAxis label="y value" orientation="left" />
        </XYChart>
      </div>
    );
  }

  componentDidMount() {
    this.props.getIris();
  };

  getIrisSeries(irisData){
    //console.log(irisData)
    
    let sepalLenSeries = irisData.map((oneIris)=>oneIris.sepal_len);
    let sepalWidthSeries = irisData.map((oneIris)=>oneIris.sepal_width);
    let petalLenSeries = irisData.map((oneIris)=>oneIris.petal_len);
    let petalWidthSeries = irisData.map((oneIris)=>oneIris.petal_width);

    return {
      sepalLenSeries,
      sepalWidthSeries,
      petalLenSeries,
      petalWidthSeries
    }
  }

  setIrisSeries(irisData){
    this.irisSeries = this.getIrisSeries(irisData);
  }

  getSepalScatterData(){
    let irisSeries = this.irisSeries;

    let sepalLenSeries = irisSeries.sepalLenSeries;
    let sepalWidthSeries = irisSeries.sepalWidthSeries;

    let data = {
      x: 'sepalLen',
      columns: [
        ["sepalLen", ...sepalLenSeries],
        ["sepalWidth", ...sepalWidthSeries]
      ],
      type: 'scatter'
    };

    console.log(data);

    return data;
  }

  getSepalScatterAxis(){
    return {
        x: {
            label: 'Sepal.Length',
            tick: {
                fit: false
            }
        },
        y: {
            label: 'Sepal.Width'
        }
    };
  }


  getPetalScatterData(){
    let irisSeries = this.irisSeries;

    let petalLenSeries = irisSeries.petalLenSeries;
    let petalWidthSeries = irisSeries.petalWidthSeries;

    let data = {
      x: 'petalLen',
      columns: [
        ["petalLen", ...petalLenSeries],
        ["petalWidth", ...petalWidthSeries]
      ],
      type: 'scatter'
    };

    console.log(data);

    return data;
  }

  getPetalScatterAxis(){
    return {
        x: {
            label: 'Petal.Length',
            tick: {
                fit: false
            }
        },
        y: {
            label: 'Petal.Width'
        }
    };
  }

  renderTimeSeries() {
    return (
      <XYChart
      ariaLabel="Description"
      xScale={{ type: "time" }}
      yScale={{ type: "linear" }}
      renderTooltip={({ datum }) => (
        <div style={{ width: 150 }}>
          <div>{formatHour(datum.x)}</div>
          <div>{formatDay(datum.x)}</div>
          <div>{formatVerbose(datum.x)}</div>
        </div>
      )}
      width={500}
      height={300}
      margin={{ left: 32, right: 32 }}
    >
      <PatternLines
        id="lines"
        height={8}
        width={8}
        stroke="#fff"
        strokeWidth={1.5}
        orientation={["diagonal"]}
      />
      <AreaSeries data={timeSeriesData} fill="url(#lines)" />
      <XAxis
        tickFormat={formatHour}
        tickLabelProps={(val, i) => ({
          textAnchor: "start",
          dy: 0,
          angle: 45
        })}
      />
    </XYChart>        
    );
  }

  render() {
    this.setIrisSeries(this.props.iris);
    
    let sepalData = this.getSepalScatterData();
    let sepalAxis = this.getSepalScatterAxis();

    let petalData = this.getPetalScatterData();
    let petalAxis = this.getPetalScatterAxis();

    const { showVoronoi } = this.state;
    const { parentWidth } = this.props;
    const size = Math.floor(parentWidth * 0.6);
    const scatterSize = Math.floor(size * 0.8);
    const histSize = size - scatterSize;    
    
    const ResponsiveHistogram = withParentSize(({ parentWidth, parentHeight, ...rest}) => (
      <Histogram
        width={parentWidth}
        height={parentHeight}
        {...rest}
      />
    ));

    const rawData11 = Array(10).fill().map(Math.random);
    //console.log(renderScatter({ width, height }));

    return (
      <Fragment>
        <h2>Iris Sepal Length</h2>
        <div style={{height:"500px"}}>
        <ResponsiveHistogram
            orientation="vertical"
            cumulative={false}
            normalized={false}
            binCount={20}
            valueAccessor={datum => datum}
            binType="numeric"
            renderTooltip={({ event, datum, data, color }) => (
              <div>
                <strong style={{ color }}>{datum.bin0} to {datum.bin1}</strong>
                <div><strong>count </strong>{datum.count}</div>
                <div><strong>cumulative </strong>{datum.cumulative}</div>
                <div><strong>density </strong>{datum.density}</div>
              </div>
            )}
          >
            <BarSeries
              rawData={this.irisSeries.sepalLenSeries /* or binnedData={...} */}
            />
            <DensitySeries
              rawData={this.irisSeries.sepalLenSeries /* or binnedData={...} */}
            />
            <XAxis />
            <YAxis />
          </ResponsiveHistogram>
        </div>

        <h2>Iris Sepal Width</h2>
        <div style={{height:"500px"}}>
        <ResponsiveHistogram
            orientation="vertical"
            cumulative={false}
            normalized={false}
            binCount={20}
            valueAccessor={datum => datum}
            binType="numeric"
            renderTooltip={({ event, datum, data, color }) => (
              <div>
                <strong style={{ color }}>{datum.bin0} to {datum.bin1}</strong>
                <div><strong>count </strong>{datum.count}</div>
                <div><strong>cumulative </strong>{datum.cumulative}</div>
                <div><strong>density </strong>{datum.density}</div>
              </div>
            )}
          >
            <BarSeries
              rawData={this.irisSeries.sepalWidthSeries /* or binnedData={...} */}
            />
            <DensitySeries
              rawData={this.irisSeries.sepalWidthSeries /* or binnedData={...} */}
            />
            <XAxis />
            <YAxis />
          </ResponsiveHistogram>
        </div>

        <h2>Iris Petal Length</h2>
        <div style={{height:"500px", transform:'rotate(90deg)'}}>
        <ResponsiveHistogram
            orientation="horizontal"
            cumulative={false}
            normalized={false}
            binCount={20}
            valueAccessor={datum => datum}
            binType="numeric"
            renderTooltip={({ event, datum, data, color }) => (
              <div>
                <strong style={{ color }}>{datum.bin0} to {datum.bin1}</strong>
                <div><strong>count </strong>{datum.count}</div>
                <div><strong>cumulative </strong>{datum.cumulative}</div>
                <div><strong>density </strong>{datum.density}</div>
              </div>
            )}
          >
            <BarSeries
              rawData={this.irisSeries.petalLenSeries /* or binnedData={...} */}
            />
            <DensitySeries
              rawData={this.irisSeries.petalLenSeries /* or binnedData={...} */}
            />
            <XAxis />
            <YAxis />
          </ResponsiveHistogram>
        </div>


        <h2>Iris Petal Width</h2>
        <div style={{height:"500px"}}>
        <ResponsiveHistogram
            orientation="vertical"
            cumulative={false}
            normalized={false}
            binCount={20}
            valueAccessor={datum => datum}
            binType="numeric"
            renderTooltip={({ event, datum, data, color }) => (
              <div>
                <strong style={{ color }}>{datum.bin0} to {datum.bin1}</strong>
                <div><strong>count </strong>{datum.count}</div>
                <div><strong>cumulative </strong>{datum.cumulative}</div>
                <div><strong>density </strong>{datum.density}</div>
              </div>
            )}
          >
            <BarSeries
              rawData={this.irisSeries.petalWidthSeries /* or binnedData={...} */}
            />
            <DensitySeries
              rawData={this.irisSeries.petalWidthSeries /* or binnedData={...} */}
            />
            <XAxis />
            <YAxis />
          </ResponsiveHistogram>
        </div>

        <h2>Iris Sepal Scatter Colored</h2>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <h3>Iris Sepal Scatter Colored</h3>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <h3>Iris Sepal Scatter Colored</h3>
                {this.renderTimeSeries()}
            </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  iris: state.iris.iris
});

//this.renderScatter({ width: "500px", height: "500px" })


export default connect(
  mapStateToProps,
  { getIris }
)(IrisExplore);

