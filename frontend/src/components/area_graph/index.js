import React from "react";
import ReactDOM from "react-dom";
import { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getIris } from "../../actions/iris";

import C3Chart from 'react-c3js';
import 'c3/c3.css';

import { XYChart, PatternLines, AreaSeries, XAxis, YAxis,
  PointSeries, } from "@data-ui/xy-chart";
import {colors, theme} from "@data-ui/theme";
import { timeParse, timeFormat } from "d3-time-format";

export const parseDate = timeParse("%Y%m%d");
export const formatHour = timeFormat("%I:%M");
export const formatDay = timeFormat("%b %d");
export const formatVerbose = timeFormat("%B %d, %Y");


//const colors = theme.colors.categories;


const timeSeriesData = [
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() }
];

const timeSeriesData1 = [
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() },
  { x: Math.random(), y: Math.random() }
];


export class AreaGraph extends Component {

    renderTimeSeries() {
      return (
        <XYChart
        ariaLabel="Description"
        xScale={{ type: "linear" }}
        yScale={{ type: "linear" }}
        renderTooltip={({ datum }) => (
          <div style={{ width: 150 }}>
            <div>{datum.x}</div>
            <div>{datum.y}</div>
          </div>
        )}
        width={500}
        height={300}
        margin={{ left: 32, right: 32 }}
      >
        <PatternLines
          id="lines"
          height={8}
          width={8}
          stroke="#000"
          strokeWidth={1.5}
          orientation={["diagonal"]}
        />
        <PointSeries data={timeSeriesData} fill="#000" opacity={0.7} size={5}  />        
        <PointSeries data={timeSeriesData1} fill="#f00" opacity={0.7} size={5}  />        
        <XAxis
          label="y value"
          tickLabelProps={(val, i) => ({
            textAnchor: "start",
            dy: 0,
            angle: 45
          })}
        />
      <YAxis label="y value" orientation="left" />
      </XYChart>        
      );
    }

    render() {
        return (this.renderTimeSeries());
    }

}

const mapStateToProps = state => ({
    iris: state.iris.iris
  });
  
  export default connect(
    mapStateToProps,
    { getIris }
  )(AreaGraph);
  
