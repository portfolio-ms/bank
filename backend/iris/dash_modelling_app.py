import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import plotly.graph_objs as go

import numpy as np
import pandas as pd
from django_plotly_dash import DjangoDash

from .entities import DatasetEntity

import plotly.figure_factory as ff
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, \
 BaggingClassifier
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score


from .entities import DatasetEntity, ClassificationModelInputDataEntity


dashboard_name1 = 'modelling'
modelling = DjangoDash(name=dashboard_name1,
                           serve_locally=True,
                           # app_name=app_name
                          )

ys = ['logistic_regression', 'knn', 'decision_tree', 'RandomForest100Estimators',
'RandomForest50Estimators', 'RandomForest25Estimators', 'BaggingClassifier', 'AdaBoostClassifier']

fpath = 'bank_data_prediction_task.csv'
bankdf = pd.read_csv(fpath, sep = ',')
dataset_entity = DatasetEntity(bankdf)
dataset_entity.preprocess()
df_campaign, df_control = dataset_entity.split_campaign_from_control()
df_campaign.drop('test_control_flag', axis=1, inplace=True)
dataset_campaign = DatasetEntity(df_campaign)

X_train, X_test, y_train, y_test = dataset_campaign.split_train_test()
classification_input = ClassificationModelInputDataEntity(
        X_train, X_test, y_train, y_test)


modelling.layout = html.Div([
    dcc.RadioItems(
        id='y-axis', 
        options=[{'value': x, 'label': x} 
                 for x in ys],
        value='logistic_regression', 
        labelStyle={'display': 'inline-block'}
    ),
    #dcc.Graph(id="confusion-matrix"),
    #html.P("Description:"),
    #html.Br(),
    #html.P("y-axis:", id='description'),
    html.Div(id='graphs')
])

def choose_model(model_name='logistic_regression'):
    cases = {
        'logistic_regression': lambda: LogisticRegression(),
        'knn': lambda: KNeighborsClassifier(),
        'decision_tree': lambda: DecisionTreeClassifier(),
        'RandomForest100Estimators': lambda: RandomForestClassifier(n_estimators= 100),
        'RandomForest50Estimators': lambda: RandomForestClassifier(n_estimators= 50),
        'RandomForest25Estimators': lambda: RandomForestClassifier(n_estimators= 25),
        'AdaBoostClassifier': lambda: AdaBoostClassifier(n_estimators= 50, learning_rate=1.0,
             random_state=22),
        'BaggingClassifier': lambda: BaggingClassifier(n_estimators= 50),
    }
    return cases[model_name]()


def draw_confusion_matrix(cm, model_name='logistic_regression'):

    '''
    z = [[0.1, 0.3, 0.5, 0.2],
         [1.0, 0.8, 0.6, 0.1],
         [0.1, 0.3, 0.6, 0.9],
         [0.6, 0.4, 0.2, 0.2]]
    '''

    x = ['0', '1',]
    y =  ['0', '1',]

    # change each element of z to type string for annotations
    z_text = [[str(y) for y in x] for x in cm]

    # set up figure 
    fig = ff.create_annotated_heatmap(cm, x=x, y=y, annotation_text=z_text, colorscale='Viridis')

    # add title
    fig.update_layout(title_text='<i><b> %s: Confusion matrix</b></i>' %model_name,
                  #xaxis = dict(title='x'),
                  #yaxis = dict(title='x')
                 )

    # add custom xaxis title
    fig.add_annotation(dict(font=dict(color="black",size=14),
                        x=0.5,
                        y=-0.15,
                        showarrow=False,
                        text="Predicted value",
                        xref="paper",
                        yref="paper"))

    tn, fp, fn, tp = cm.ravel()
    n_called = (tp+fp)

    model_conversion = tp/n_called


    fig.add_annotation(dict(font=dict(color="black",size=14),
                        x=-0.35,
                        y=-0.25,
                        showarrow=False,
                        text="Conversion: %.3f" %model_conversion,
                        xref="paper",
                        yref="paper"))

    # add custom yaxis title
    fig.add_annotation(dict(font=dict(color="black",size=14),
                        x=-0.35,
                        y=0.5,
                        showarrow=False,
                        text="Real value",
                        textangle=-90,
                        xref="paper",
                        yref="paper"))

    # adjust margins to make room for yaxis title
    fig.update_layout(margin=dict(t=50, l=200))

    # add colorbar
    fig['data'][0]['showscale'] = True
    #fig.show()
    return fig


def draw_roc_curve_dash(model, X_test, y_test):
    logit_roc_auc = roc_auc_score(y_test, model.predict(X_test))
    fpr, tpr, thresholds = roc_curve(y_test, 
        model.predict_proba(X_test)[:,1])

    # Create a trace
    traces = go.Figure()
    trace = go.Scatter(x=fpr,
                       y=tpr, label='ROC curve')
    
    traces.add_trace(trace)

    base_trace = go.Scatter(x=[0.0,1.0],
                       y=[0.0,1.0])
    traces.add_trace(base_trace)
    auc_score = roc_auc_score(y_test, model.predict_proba(X_test)[:,1])

  

    #data = [trace, base_trace]
    data = traces
    
    traces.layout = dict(
        title='ROC Curve',
        yaxis=dict(zeroline=False, title='True Positive Rate',),
        xaxis=dict(zeroline=False, title='False Positive Rate',
                   tickangle=0),
                  margin=dict(t=40, b=50, l=50, r=40),
                  height=350,
                 )
    traces.add_annotation(dict(font=dict(color="black",size=14),
                        x=0,
                        y=0,
                        showarrow=False,
                        text="AUC score: %.3f" %auc_score,
                        xref="paper",
                        yref="paper"))                   
    #fig = dict(data=data, layout=layout)
    fig = traces

    line_graph = dcc.Graph(id='line-area-graph2',
        figure=fig, style={'display':'inline-block', 'width':'100%',
                           'height':'100%;'})    

    return line_graph
'''
@modelling.expanded_callback(
    Output("confusion-matrix", "figure"),  
     [Input("y-axis", "value")])
def generate_fig(y):
    # things
    fig = make_fig(y)
    return fig
'''

@modelling.expanded_callback(
    Output('graphs', 'children'),
    [Input("y-axis", "value")]
)
def generate_graphs(model_name):
    model = choose_model(model_name)
    model, score_train, score_test, report, confusion_matrix = \
         classification_input.fit_evaluate_report(model)
    
    cm_figure = draw_confusion_matrix(confusion_matrix, model_name)

    roc_curve = draw_roc_curve_dash(model, X_test, y_test)

    children = [dcc.Graph(id="confusion-matrix", figure = cm_figure),
                html.Br(),
                html.Div([roc_curve])]

    return children

