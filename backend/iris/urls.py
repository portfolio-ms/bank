from rest_framework import routers
from .api import IrisViewSet, IrisTrain, IrisPredict
from .views import dash_example_1_view, dash_boxplot1_view, dash_modelling_view

from django.urls import path, include
import iris.dash_apps      # pylint: disable=unused-import
import iris.dash_app      # pylint: disable=unused-import
import iris.dash_modelling_app      # pylint: disable=unused-import



router = routers.DefaultRouter()
router.register('api/iris', IrisViewSet, 'iris')

urlpatterns = [
    path('api/train',  IrisTrain.as_view()),
    path('api/predict',  IrisPredict.as_view()),
    #path('demo-six', dash_example_1_view, name="demo-six"),    
    path('boxplot1', dash_boxplot1_view, name="boxplot1"),    
    path('modelling', dash_modelling_view, name="modelling"),    
]

urlpatterns += router.urls