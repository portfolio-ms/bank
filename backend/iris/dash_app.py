import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import plotly.graph_objs as go

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, \
 BaggingClassifier

import numpy as np
import pandas as pd
from django_plotly_dash import DjangoDash

dashboard_name1 = 'boxplot1'
boxplot1 = DjangoDash(name=dashboard_name1,
                           serve_locally=True,
                           # app_name=app_name
                          )
from .entities import DatasetEntity



#app = dash.Dash(__name__)
#df = px.data.tips()
fpath = 'bank_data_prediction_task.csv'
bankdf = pd.read_csv(fpath, sep = ',')
dataset_entity = DatasetEntity(bankdf)
dataset_entity.preprocess()
df_campaign, df_control = dataset_entity.split_campaign_from_control()
df_campaign.drop('test_control_flag', axis=1, inplace=True)
dataset_campaign = DatasetEntity(df_campaign)
#store or cache (later)
#put in database for now


def dict_to_text(dictionary):
    rows = sorted([(dictionary[key], key) for key in dictionary])
    txt = '\n'.join(['%d - %s' %(row[0], row[1]) for row in rows])
    return txt


#j.savefig('comparison_outcome_by_retargeting.png')

ys = ['age', 'job', 'marital', 'education', 'default', 'housing', 'loan', 'poutcome',
          'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m', 'nr.employed']


boxplot1.layout = html.Div([
    dcc.RadioItems(
        id='y-axis', 
        options=[{'value': x, 'label': x} 
                 for x in ys],
        value='previous', 
        labelStyle={'display': 'inline-block'}
    ),
    dcc.Graph(id="box-plot"),
    html.P("Description:"),
    html.Br(),
    html.P("y-axis:", id='description'),
])


@boxplot1.expanded_callback(
    Output("box-plot", "figure"),  
     [Input("y-axis", "value")])
def generate_chart(y):
    fig = px.box(df_campaign, x = 'y', y =y)
    return fig

@boxplot1.expanded_callback(
    Output("description", "children"),  
     [Input("y-axis", "value")])
def update_description(y):
    poutcome_n = {'success':2, 'failure':1, 'nonexistent':0}
    job_n = {"entrepreneur": 1, "management": 2, "technician": 3, "admin.": 4, "services": 5,
             "self-employed": 6, "blue-collar": 7, "retired": 8, "unemployed": 9,
             "housemaid": 10,  "student": 11, "unknown": 12 }
    edu_n = {"basic.4y": 1, "basic.6y":2, "basic.9y":3, "high.school":4,"illiterate":5,
             "professional.course":6, "university.degree":7, "unknown":8}
    housing_n = {'yes':1, 'no':0, 'unknown':0}
    default_n = {'yes':1, 'no':0, 'unknown':0}
    loan_n = {'yes':1, 'no':0, 'unknown':0}
    y_n = {'yes':1, 'no':0}
    marital_n = {'single':0, 'married':1, 'divorced': 2, 'unknown':3}

    descriptions = {'age': "Client's age",
                    
                    'job': 'job type of job (categorical: %s' %dict_to_text(job_n),
                    'marital': 'marital status (categorical: %s; note: "divorced" means divorced or widowed)' %dict_to_text(marital_n),
                    'education':'(categorical: %s' %dict_to_text(edu_n),
                    'default': 'has credit in default? (categorical: %s)' %dict_to_text(default_n),
                    'housing': 'has housing loan? (categorical: %s)' %dict_to_text(housing_n),
                    'loan': 'has personal loan? (categorical: %s)' %dict_to_text(loan_n),
                    'test_control_flag': 'contains information if the person was part of the campaign ("campaign group") or control group ("control group"). Campaign group was called (details on the contact below) and offered term deposit. All customers could subscribe to the deposit (also control group)',
                    'y': 'has the client subscribed a term deposit? (binary: %s)' %dict_to_text(y_n),
                    'poutcome': 'outcome of the previous marketing campaign'+\
                    ' (categorical: %s)' %dict_to_text(poutcome_n),
                    'emp.var.rate': 'employment variation rate - quarterly indicator (numeric)',
                    'cons.price.idx': 'consumer price index - monthly indicator (numeric)',
                    'cons.conf.idx': 'consumer confidence index - monthly indicator (numeric)',
                    'euribor3m': 'euribor 3 month rate - daily indicator (numeric)',
                    'nr.employed': 'number of employees - quarterly indicator (numeric)',
                    'previous': 'number of contacts in previous campaign'

                    }
    description = descriptions.get(y)
    if description is None:
        description = 'No description available'
    return "{}: {}".format(y, description)

#if __name__ == "__main__":
#    app.run_server(debug=True)
