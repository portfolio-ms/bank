import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report, \
 plot_confusion_matrix

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, \
 BaggingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_curve, roc_auc_score


class DatasetEntity(object):
    
    def __init__(self, df):
        self.df = df
    
    def split_campaign_from_control(self, ):
        df_campaign = self.df[self.df['test_control_flag']=='campaign group']
        df_control = self.df[self.df['test_control_flag']=='control group']
        return df_campaign, df_control
    
    def split_train_test(self, test_size=0.3):
        #creting X and y variable 
        X = self.df.drop('y', axis=1)
        y = self.df['y']
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size = test_size, random_state =1)
        return X_train, X_test, y_train, y_test
    
    def drop_non_personal(self):
        return
    
    def preprocess(self):
        self.df['poutcome'].replace(
            {'success':2, 'failure':1, 'nonexistent':0}, inplace=True)
        jobs_by_expected_income = {
            'entrepreneur':1, 'management':2,'technician':3,'admin.':4,
            'services':5, 'self-employed':6,'blue-collar':7,'retired':8,
            'unemployed':9,'housemaid':10,'student':11,'unknown':12}
        self.df['job'].replace(jobs_by_expected_income, inplace=True)
        self.df['education'].replace(
            {
                "basic.4y": 1, "basic.6y":2, "basic.9y":3, "high.school":4,"illiterate":5,
             "professional.course":6, "university.degree":7, "unknown":8
             }, inplace=True)

        self.df['housing'].replace({'yes':1, 'no':0, 'unknown':0}, inplace=True)
        self.df['default'].replace({'yes':1, 'no':0, 'unknown':0}, inplace=True)
        self.df['loan'].replace({'yes':1, 'no':0, 'unknown':0}, inplace=True)

        self.df['marital'].replace({'single':0, 'married':1, 'divorced': 2, 'unknown':3}, inplace=True)
        
        self.df['y'].replace({'yes':1, 'no':0}, inplace=True)
        columns_to_drop = ['day_of_week', 'month', 'contact', 'duration']
        for column in columns_to_drop:
            self.df.drop(column, axis=1, inplace=True)
        return
    
    def conversion(self):
        return self.df['y'].sum()/self.df.shape[0]


class ClassificationModelInputDataEntity(object):
    
    def __init__(self, X_train, X_test, y_train, y_test):
        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test
        return
    
    def fit(self, model):
        model.fit(self.X_train, self.y_train)
        return model, model.score(self.X_train, self.y_train)

    def evaluate(self, model):
        return model.score(self.X_test, self.y_test)

    def report(self, model):
        pred = model.predict(self.X_test)
        return classification_report(pred, self.y_test)
    
    def confusion_matrix(self, model):
        pred = model.predict(self.X_test)
        return confusion_matrix(self.y_test, pred)
    
    def fit_evaluate_report(self, model):
        model, score_train = self.fit(model)
        score_test = self.evaluate(model)
        report = self.report(model)
        confusion_matrix = self.confusion_matrix(model)
        return model, score_train, score_test, report, confusion_matrix
    
    def compare(self, models):
        comparison = []
        for model in models:
            model_fitting_results = self.fit_evaluate_report(model)
            comparison.append(model_fitting_results)
        return comparison
    
    def compare_popular(self):
        models = []
        names = []

        lr = LogisticRegression()
        models.append(lr)
        names.append('logistic_regression')

        knn = KNeighborsClassifier()
        models.append(knn)
        names.append('knn')

        dtree = DecisionTreeClassifier()
        models.append(dtree)
        names.append('dtree')
        
        rf100 = RandomForestClassifier(n_estimators= 100)
        models.append(rf100)
        names.append('RandomForest100')

        rf50 = RandomForestClassifier(n_estimators= 50)
        models.append(rf50)
        names.append('RandomForest50')
        
        rf25 = RandomForestClassifier(n_estimators= 25)
        models.append(rf25)
        names.append('RandomForest25')

        adb = AdaBoostClassifier(n_estimators= 50, learning_rate=1.0, random_state=22)
        models.append(adb)
        names.append('AdaBoostClassifier')

        bagging_classifier = BaggingClassifier(n_estimators= 50)
        models.append(bagging_classifier)
        names.append('Bagging Classifier')
        return names, self.compare(models)

def function():
    fpath = 'bank_data_prediction_task.csv'
    df = pd.read_csv(fpath, sep = ',')
    dataset_entity = DatasetEntity(df)
    dataset_entity.preprocess()
    df_campaign, df_control = dataset_entity.split_campaign_from_control()
    df_campaign.drop('test_control_flag', axis=1, inplace=True)
    dataset_campaign = DatasetEntity(df_campaign)
    dataset_control = DatasetEntity(df_control)

    campaign_conversion = dataset_campaign.conversion()
    campaign_retargeted = df_campaign[df_campaign['poutcome']>0]
    rt_conversion = DatasetEntity(campaign_retargeted).conversion()
    rt_lift = rt_conversion/campaign_conversion

    campaign_retargeted_success = df_campaign[df_campaign['poutcome']==2]
    rts_conversion = DatasetEntity(campaign_retargeted_success).conversion()
    rts_lift = rts_conversion/campaign_conversion
    
    ## Studying distribution of values
    g = sns.boxenplot(df_control['age'])
    g.savefig('control_age_distribution.png')

    h = sns.boxenplot(df_campaign['age'])
    h.savefig('campaign_age_distribution.png')

    i = sns.distplot(df_campaign['age'], bins=10, kde=False)
    i.savefig('campaign_age_distribution_bar.png')

    j = sns.boxenplot(x = 'y', y ='previous', data=df )
    j.savefig('comparison_outcome_by_retargeting.png')

    X_train, X_test, y_train, y_test = dataset_campaign.split_train_test()
    classification_input = ClassificationModelInputDataEntity(
        X_train, X_test, y_train, y_test)

    
    rows = []
    reports = []
    cms = []
    models = []
    names, results = classification_input.compare_popular()
    for i in range(len(results)):
        model, score_train, score_test, report, cm = results[i]
        row = [names[i], score_train, score_test]
        reports.append(report)
        cms.append(cm)
        rows.append(row)
        models.append(model)
    
    df_scores = pd.DataFrame(rows, columns = ['name', 'score_train', 'score_test']).sort_values(
    'score_test', ascending=False)

    cm_plot = plot_confusion_matrix(models[names.index('AdaBoostClassifier')], X_test, y_test)

    ada_model = models[names.index('AdaBoostClassifier')]
    logit_roc_auc = roc_auc_score(y_test, ada_model.predict(X_test))
    fpr, tpr, thresholds = roc_curve(y_test, ada_model.predict_proba(X_test)[:,1])
    plt.figure()
    plt.plot(fpr, tpr, label='AdaBoostClassifier Score (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    auc_score = roc_auc_score(y_test, ada_model.predict_proba(X_test)[:,1])
    print("AdaBoostClassifier Score AUC Score:",auc_score)

    tn, fp, fn, tp = cms[names.index('AdaBoostClassifier')].ravel()
    n_called = (tp+fp)

    model_conversion = tp/n_called
    model_lift = model_conversion/dataset_campaign.conversion()
    return

